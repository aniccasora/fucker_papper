from tensorflow.keras import layers
import tensorflow as tf
from segQR_data_Loader import SegQRCodeDataLoader as DataLoader # 我的 data loader
import numpy as np
import glob
from PIL import Image, ImageOps
from skimage.filters import threshold_sauvola
import cv2
import math

   
def get_subimage_by_windows_params(image, sub_windows_params, isColorful=True):
    
    """
    根據給定的 window size 來切出 所有 slide windows 所走訪的圖片。
    --------------------------
    image: 要被走訪的圖片。
    
    sub_windows_params: 所有要走的子區域大小、座標。
    透過 get_each_windows_status() 所得出的資料
    
    isColorful(opt): 預設處理 三色圖片。
    
    return ndarray: 所有區域的圖片
    """
    # 取得圖片副本
    image = np.asarray(image, dtype=np.uint8)
    
    if isColorful and len(image.shape) != 3:
        if image.shape[2] != 3 :
            raise Exception("input image expected is rgb image, but get shape={}".format(image.shape))
        else:
            raise Exception("input image.shape ={}".format(image.shape))
    
    (stri_x, stri_y) = sub_windows_params['stride']
    (size_w, size_h) = sub_windows_params['mask_size']
    coordinates = sub_windows_params['coordi']
    
    rtn = []
    if isColorful:
        for x_y in coordinates:
            mask = _get_sp_mask(x_y, (size_w, size_h), entire_size=(image.shape[0:2]))
            sub_rgb = _apply_mask_2(image,mask)
            rtn.append(sub_rgb)
    else:
        raise Exception("處理 黑白圖片尚未實作...")
    
    return np.asarray(rtn)


def _apply_mask_2(image, mask, isRGB=True):
    """
    將遮罩 使用在 Rgb(預設) 的 image 上面。
    ------------------------------
    image: 彩色的圖片。
    
    mask(ndarray, dtype=bool): 2-d mask array。
    這其實是為了給同心方形mask用的。
    
    isRGB(default=True): 預設 image 是彩色的。
    ------------------------------
    return ndarray of 3-chennels by assign mask.
    
    """
    if image.shape[2] != 3:
        raise Exception("Image is not 3 chennels image. Shape={}".format(image.shape))
    
    if image.shape[0:2] != mask.shape:
        raise Exception("image shape={} is not equvelant mask shape={}".format(image.shape,mask.shape))
    
    # 對 mask 有效區域"數量" 開平方。(有效 = mask value 是 true)
    best_output_side = round(math.sqrt(np.sum(mask)))
    
    # 套用過 mask 以後會變成 1-d 數據要再次reshape成 2-d。
    bs_shape=(best_output_side,best_output_side)
    
    rtn = None
    if isRGB:
        r = image[:,:,0]
        g = image[:,:,1]
        b = image[:,:,2]

        masked_r = r[mask].reshape(bs_shape)
        masked_g = g[mask].reshape(bs_shape)
        masked_b = b[mask].reshape(bs_shape)
        
        rtn = np.dstack((masked_r,masked_g,masked_b))
        
    else:
        raise Exception("尚未實作 isRGB=False")
    
    return rtn


def get_each_subwindows_params(image, mask_size, stride):
    """
    回傳 {"stride":...,"mask_size":...,"coordi":...}
    
    會依據 給定的 mask_size, stride，回傳所有 sub-region 座標 等其他定位參數，
    方便 mask function 取得子圖。
    
    --- 細部實作 function 原因 ---
    因為透過 get_sp_mask() 拿連續子圖還是需要 一些參數的 (位置, 大小)，
    這個 function 的目的就是生成這些數值組， 然後丟給 get_sp_mask()
    生成子圖遮罩。
    ---------------------
    image : 原圖
    
    mask_size : slide window 的大小，子圖的大小，可指定(w,h), 或者都等長度(int)
    
    stride : 每個 windows 每次跳躍的間隔 (int) 或者 (x_stride,y_stride)。
    
    ---------------------
    
    return dict : {"stride":(stri_x,stri_y),"mask_size":(mask_x,mask_y),"coordi":all_combination}
    
    """
    
    mask_x, mask_y = 0, 0 # windows size
    if isinstance(mask_size, int):
        mask_x, mask_y = mask_size, mask_size
    else:
        mask_x, mask_y = mask_size[0], mask_size[1]
        
    stri_x, stri_y = 0, 0 # windows 每次移動的步伐
    if isinstance(stride, int):
        stri_x, stri_y = stride, stride
    else:
        stri_x, stri_y = stride[0], stride[1]
        
    img_w, img_h = image.shape[1], image.shape[0]
    
    # 橫向移動、垂直移動的 範圍，mask 的左上角為 mask 的 location。
    x_range = img_w - mask_x 
    y_range = img_h - mask_y 
    
    # mask 可以存在的位置
    x_potential, y_potential = [], []
    
    x_potential = [x*stri_x for x in range((x_range//stri_x) +1)] # +1: 因為 原點也要算入。  
    y_potential = [y*stri_y for y in range((y_range//stri_y) +1)]
    
    # 防止我上面的兩個 for 寫錯的檢核。
    if x_potential[-1] + mask_x > img_w:
        raise Exception("x_potential[-1] + mask_x >= img_weight:  {} >= {}".
                        format(x_potential[-1] + mask_x,img_w))
        
    if y_potential[-1] + mask_y > img_h:
        raise Exception("x_potential[-1] + mask_x >= img_weight:  {} >= {}".
                        format(y_potential[-1] + mask_y,img_h))
    
    all_combination = []
    # 組合
    for y in y_potential:
        for x in x_potential:
            all_combination.append((y,x)) # 此輸出是用 matrix 坐標系
            
    return {"stride":(stri_x,stri_y),"mask_size":(mask_x,mask_y),"coordi":all_combination}


def _get_sp_mask(location, sub_size, entire_size, sub_value=True, global_vlaue=False):
    """
    生成一個具有 sub-mask 的 mask。
    
    想像一下在 一張白紙 畫一個 同心方框 其中 外部的區域塗黑(預設行為)，
    return mask 就是長的想像的樣子。
    -------------------------------
    location (int,int): mask 要開始填入的位置 (0-base, matrix-coordi)
    
    sub_size(rows, cols) : sub-mask 的大小 or (int)
     
    entire_size (rows, cols): 整個 mask 的大小  or (int)。
    
    sub_value(opt) : sub region 填入的數值。(ndarray 可接受的就行)
    
    global_vlaue(opt) : 沒有被 sub region 所包含的 區域數值。
    -------------------------------
    
    @return: ndarray(dtype=bool).
    
    """
    # for sub
    r, c = 0, 0
    if isinstance(sub_size,int):
        r, c = sub_size, sub_size
    else:
        r, c = sub_size[0], sub_size[1]
    
    # entire
    R, C = 0, 0
    if isinstance(entire_size,int):
        R, C = entire_size, entire_size
    else:
        R, C = entire_size[0], entire_size[1]
    
    _ = None
    if global_vlaue == False:
        _ = np.zeros((R,C), dtype=bool)
    else:
        _ = np.ones((R,C), dtype=bool)
    
    _[location[0]:location[0]+r,location[1]:location[1]+c] = sub_value
    
    return _


def eat_image_2_pyramid(img: np.ndarray, threshold_window_size=25, pyramid_factor=0.7, number_of_layer = 4):
    """
    把一張圖 轉成 n 層的 image pyramid，最底層的大小就是原圖。
    
    img : 原圖。請轉成 ndarray。
    
    ... : 剩下參數自己猜。
    
    rtn : list of ndarray.
    """
    #### rename area
    factor = pyramid_factor
    window_size = threshold_window_size
    ####
    
    img = Image.fromarray(img)
    img = ImageOps.grayscale(img)
    img = np.asarray(img)
    thres_sauvola = threshold_sauvola(image=img, window_size=25) # 計算出 threshold(float)
    binary_sauvola = img > thres_sauvola # "二值化"(return is "boolean mask")
    layer = np.asarray(binary_sauvola, dtype=np.uint8) # boolean -> uint8
    rtn = []
    for i in range(number_of_layer):
        # using pyrDown() function 
        rtn.append(layer)
        cols, rows = layer.shape[1::-1]
        layer = cv2.resize(layer, dsize=(round(cols*factor), round(rows*factor)), interpolation=cv2.INTER_AREA)
    return rtn


def get_datas_X_y(tar_pth, quiet=False):
    """
    取得訓練資料集(X, y)
    quiet : 是否要印出 load 的資料夾路徑。
    
    return X, y
    """
    X, y = None, None

    for idx, seg_path in enumerate(glob.glob(tar_pth+'\\*')):

        _X, _y = DataLoader(seg_path, quiet=quiet).get_X_y()
        if idx == 0:
            X = _X
            y = _y
        
        else:
            X = np.append(X,_X,axis=0)
            y = np.append(y,_y,axis=0)
        
    return X, y


def get_model():
    
    inl = layers.Input((32, 32, 3))
    out = inl

    out = layers.Conv2D(filters=6, kernel_size=(5, 5), padding='valid', activation='relu')(out)
    out = layers.BatchNormalization()(out)
    out = layers.MaxPooling2D(pool_size=(2, 2))(out)
    out = layers.Dropout(0.3)(out)

    out = layers.Conv2D(filters=12, kernel_size=(5, 5), padding='valid', activation='relu')(out)
    out = layers.BatchNormalization()(out)
    out = layers.MaxPooling2D(pool_size=(2, 2))(out)
    out = layers.Dropout(0.3)(out)

    # 全連接層
    out = layers.Flatten()(out)
    out = layers.Dropout(0.3)(out)

    out = layers.Dense(2, name='TF', activation='softmax')(out)

    myModel = tf.keras.Model(inputs= inl, outputs = out)
    myModel.compile(optimizer='adam',
                    loss="categorical_crossentropy",
                    metrics=['accuracy'])

    return myModel