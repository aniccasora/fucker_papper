from PIL import Image
import numpy as np
import glob, os, csv
import shutil


def yes_or_no(question):
    """
    Y or N Question.
    """
    answer = input(question + "(y/n): ").lower().strip()
    print("")
    while not (answer == "y" or answer == "yes" or
               answer == "n" or answer == "no"):
        print("Input yes or no")
        answer = input(question + "(y/n):").lower().strip()
        print("")
    if answer[0] == "y":
        return True
    else:
        return False


def make_square(im, min_size=512, fill_color=(0, 0, 0)):
    """
    im : 原始圖片 (PIL.image)
    min_size : 最小正方形的單邊長度(pixel)
    fill_color : 如要擴大圖片，多出來部分所填補的顏色(預設:黑色 RGB)。

    圖片不會被 "縮小而失真"，且回傳圖片一定每邊長度至少是 min_size。

    return : 一個正方形的 PIL.image
    """
    x, y = im.size
    size = max(min_size, x, y)
    new_im = Image.new('RGB', (size, size), fill_color)
    new_im.paste(im, (int((size - x) / 2), int((size - y) / 2)))
    return new_im


def resize_square_image(im, resize=500):
    """
    im :(PIL.Image)
    resize : 縮放的邊長大小。

    將 im 縮放成 resize by resize 大小。

    return :一個正方形的 PIL.image
    """
    x, y = im.size
    if x != y:
        raise Exception("Non square image {}x{}.".format(x, y))

    return im.resize((resize, resize), Image.ANTIALIAS)


def img_segment(image, size, stride, limit=5000):
    """
    image (array-like): 欲切割原圖。
    size (int): 子圖大小，一律正方型。
    stride (int): 每個子圖所間隔的距離,一般來說會跟size相同。
    limit (int): 子圖極限。# 防止記憶體爆炸，子圖不產生超過限制。

    將任何一張圖片透過 slideing window 方式生成子圖。

    """

    im = np.asarray(image)
    w, h = im.shape[1::-1]
    ret = []  # 回傳的子圖 (list).

    # non-overlapping 走訪
    w_interval = []
    sign = 0  # 切割點
    while w > sign:
        if (sign + size) > w:
            break
        w_interval.append(sign)
        sign += stride
    # =========================
    h_interval = []
    sign = 0  # 切割點
    while h > sign:
        if (sign + size) > h:
            break
        h_interval.append(sign)
        sign += stride

    # 防止記憶體爆炸
    if len(h_interval) * len(w_interval) > limit:
        print("Oops 切出太多子圖了，切出了 {} 張，少一點~,限制={}張".format(len(h_interval) * len(w_interval), limit))
        raise Exception("Too much subimage~ Please 調整 \"size\" or \"stride\".")

    for h_sign in h_interval:
        for w_sign in w_interval:
            ret.append(im[h_sign:h_sign + size, w_sign:w_sign + size])

    return ret

def eat_image_2_pyramid(img: np.ndarray, threshold_window_size=25, pyramid_factor=0.7, number_of_layer = 4):
    """
    img : 原圖
    
    ... 剩下參數自己猜
    
    rtn : list of ndarray
    """
    #### rename area
    factor = pyramid_factor
    window_size = threshold_window_size
    ####
    
    img = Image.fromarray(img)
    img = ImageOps.grayscale(img)
    img = np.asarray(img)
    thres_sauvola = threshold_sauvola(image=img, window_size=25) # 計算出 threshold(float)
    binary_sauvola = img > thres_sauvola # "二值化"(return is "boolean mask")
    layer = np.asarray(binary_sauvola, dtype=np.uint8) # boolean -> uint8
    rtn = []
    for i in range(number_of_layer):
        # using pyrDown() function 
        rtn.append(layer)
        cols, rows = layer.shape[1::-1]
        layer = cv2.resize(layer, dsize=(round(cols*factor), round(rows*factor)), interpolation=cv2.INTER_AREA)
    return rtn


if __name__ == '__main__':
    """
    SAVE_SEGMENT_IMAGES_PATH: 分割後個圖片要放置的資資料夾位置，
                              預設會拼成 絕對路徑，可指定不存在的資料夾，他會幫建立。
    SOURCE_IMAGES_PATH: 原始 QR Code 的存放路徑，檔名將會是資料夾名稱。
    
    將 SOURCE_IMAGES_PATH 內的圖片，若檔案名稱為 {ABC.[jpg|png|或其他]}，
    程式會將此檔名改成 {ABC_[jpg|png|或其他]} 的資料夾名稱 並放置在
    SAVE_SEGMENT_IMAGES_PATH 底下。
    
    """
    # 輸出 資料夾
    SAVE_SEGMENT_IMAGES_PATH = r"C:\Users\mslab\Desktop\segmented_qr_code"
    # 來源 資料夾，這邊只能放 "圖片"，不得有非圖片的放在這邊。
    # 且 不得在 "來源 資料夾" 內創立 "輸出 資料夾"。
    SOURCE_IMAGES_PATH = r'C:\Users\mslab\Desktop\seg_non_watched'  # os.getcwd() + "\\data\\qr-code"

    if SOURCE_IMAGES_PATH.split('\\')[-1] in SAVE_SEGMENT_IMAGES_PATH.split('\\'):
        raise Exception("不得在 來源資料夾 內 指定(或創立) 輸出資料夾 !! :\n"+
                        "\t> 輸出 資料夾 :{}\n".format(SAVE_SEGMENT_IMAGES_PATH) +
                        "\t> 來源 資料夾 :{}\n".format(SOURCE_IMAGES_PATH))

    # 使用者確認路徑
    if yes_or_no("請確認 以下兩個路徑是正確的:\n"+
                 "\t> 輸出 資料夾 :{}\n".format(SAVE_SEGMENT_IMAGES_PATH)+
                 "\t> 來源 資料夾 :{}\n".format(SOURCE_IMAGES_PATH)):
        pass
    else:
        exit(8787)

    # 使否會強制覆蓋既有的 segmented image folder.
    FLAG_OF_INSTEAD_FOLDER = False

    # if not os.path.exists(SAVE_SEGMENT_IMAGES_PATH):
    #     raise Exception("Please check PATH is exists : {}".format(SAVE_SEGMENT_IMAGES_PATH))


    # 決定是否要刪除既有的 folder
    if os.path.exists(SAVE_SEGMENT_IMAGES_PATH):  # 存放資料夾是否已經有檔案?
        if FLAG_OF_INSTEAD_FOLDER is False:  # 預設不取代時候
            if yes_or_no("確定要覆蓋已存在的資料夾 -> \'{}\'?\n".format(SAVE_SEGMENT_IMAGES_PATH)):
                FLAG_OF_INSTEAD_FOLDER = True
                print("\"{}\" have exists, delete this path...\n\n".format(SAVE_SEGMENT_IMAGES_PATH))
                shutil.rmtree(SAVE_SEGMENT_IMAGES_PATH)
                os.makedirs(SAVE_SEGMENT_IMAGES_PATH)
                print("Create \"{}\" successfully.".format(SAVE_SEGMENT_IMAGES_PATH))
            else:
                pass  # 保持旗標設定
        else:  # 預設取代時候
            if yes_or_no("確定要覆蓋已存在的資料夾 -> \'{}\'?\n".format(SAVE_SEGMENT_IMAGES_PATH)):
                FLAG_OF_INSTEAD_FOLDER = True
                print("\"{}\" have exists, delete this path...".format(SAVE_SEGMENT_IMAGES_PATH))
                shutil.rmtree(SAVE_SEGMENT_IMAGES_PATH)
                os.makedirs(SAVE_SEGMENT_IMAGES_PATH)
                print("Create \"{}\" successfully.".format(SAVE_SEGMENT_IMAGES_PATH))
            else:
                pass  # 保持旗標設定
    else:
        # print("Create", SAVE_SEGMENT_IMAGES_PATH, "...")
        os.makedirs(SAVE_SEGMENT_IMAGES_PATH)
        print("Create \"{}\" successfully.".format(SAVE_SEGMENT_IMAGES_PATH))

    images_pth_list = glob.glob(SOURCE_IMAGES_PATH + "\\*")

    if len(images_pth_list) == 0:
        raise Exception(SOURCE_IMAGES_PATH+str(" ,沒有此路徑。"))

    # 等等要創的資料夾名稱
    folder_name = []

    for pth in images_pth_list:
        folder_name.append(pth[pth.rfind("\\") + 1:].replace('.', '_'))

    folder_imgPth_zip = zip(folder_name, images_pth_list)

    # 計數器
    create_action_ = 0
    skip___action_ = 0

    #  邊切圖片 邊創資料夾
    for folder, img_pth in folder_imgPth_zip:
        tar_pth = SAVE_SEGMENT_IMAGES_PATH + "/" + folder
        if os.path.isdir(tar_pth):
            if FLAG_OF_INSTEAD_FOLDER is False:  # 是否不取代
                print("> 略過 {} 資料夾...".format(folder))
                skip___action_ += 1
                continue  # 跳過既有資料夾
            else:
                shutil.rmtree(tar_pth)  # 刪除既有資料夾

        os.makedirs(tar_pth)
        create_action_ += 1

        # 切換至已創建目錄
        oldwd = os.getcwd()
        os.chdir(tar_pth)
        print("current: \"{}\" created.".format(os.getcwd()))

        # =================================================== < 子圖生成區塊>
        # 可以直接丟 segmented 的 圖 跟 {圖片名}.csv 檔案了
        im = Image.open(img_pth)
        im = make_square(im)  # 轉正方形
        im = resize_square_image(im,1000)  # 將大小調至 500

        weget = img_segment(im, size=32, stride=32)  # 取子圖
        weget = [Image.fromarray(a) for a in weget]  # ndarray -> PIL.Image

        # =================================================== </子圖生成區塊>

        sub_image_name = []  # csv 紀錄 label 用
        for idx, image in enumerate(weget):
            fname = "qq_" + str(idx) + ".jpg"  # 這個 "qq_" prefix 別再做修改了，後面的程式是寫死 "qq_" 的
            image.save(fname)
            sub_image_name.append(fname)

        # 創造 csv檔案，預先放入 ALL "Zero" = background
        with open("uncheck-" + folder + ".csv", 'w', newline='') as file:
            writer = csv.writer(file)
            for im_name in sub_image_name:
                writer.writerow([im_name, "0"])

        # 回至原來/目錄
        os.chdir(oldwd)
    # loop END~~~~~

    print("\tCreate {}, skip {}.".format(create_action_, skip___action_))
