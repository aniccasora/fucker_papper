import os
import glob
import pandas as pd
import numpy as np
from PIL import Image

class SegQRCodeDataLoader:
    """
    This Class is a Data Loader of 已經放置 "segment QR Code" 和 {資料夾名}.csv的label檔。

    * special feature: 他不會 load 全黑(0,0,0) image(32x32) 到 data 裡面，因為最初在生產圖片時有些圖片會 padding 黑邊。
    """
    def __init__(self, absolute_folder_path, quiet=False):
        """
        absolute_folder_path: 
        
        quiet : 是否要印出 load 的資料夾路徑。
        """
        oldcwd = os.getcwd()
        try:
            if not os.path.isdir(absolute_folder_path):
                raise Exception("Not exists PTAH\""+absolute_folder_path+"\"")
            
            os.chdir(absolute_folder_path)
            if not quiet:
                print("Load folder from =>",absolute_folder_path)

            csv_name = glob.glob('*.csv')
            if len(csv_name)<=0:
                raise Exception("Not any CSV file in \""+absolute_folder_path+"\" !!")
            elif len(csv_name)>1:
                raise Exception("Too many CSV found :"+ str(csv_name) + " !!")
            else:
                # 檢查 cvs fname && Folder name
                #if os.getcwd().split('\\')[-1]+".csv" == csv_name[0]:
                # 
                folder_name_p_csv = os.path.split(os.getcwd())[1]+".csv"
                if folder_name_p_csv == csv_name[0]:
                    pass
                else:
                    print("\nWarning: cvs NOT match with folder name:")
                    #print("\tFolder =",os.getcwd().split('\\')[-1])
                    print("\tFolder =",folder_name_p_csv)
                    print("\tcsv    =",csv_name[0])
            
            self.csv_name = csv_name[0]
            self.X = None # images
            self.y = None # labels (onehot format)
            
            self.__load()
            
        finally:
            os.chdir(oldcwd) # nessary!!
    
    def __toOneHot(self, text, LETTERSTR = "01"):
        '''
        text: 只有 LETTERSTR字集內 組成的字串。

        return: 回傳 one-hot encoding.
        '''
        onehot_array = []
        for letter in text:
            onehot = [0 for _ in range(2)]
            num = LETTERSTR.find(letter)
            onehot[num] = 1
            onehot_array.append(onehot)

        return np.asarray(onehot_array)

    def __load(self):
        """
        準備 label,images，基於 csv 裡面的紀錄。
        
        # In fact, cwd is images folder.
        """
        
        # 讀取 csv
        csv = pd.read_csv(self.csv_name, index_col=0, header=None)     
        onehot = [ self.__toOneHot(str(val[0])) for val in csv.values]
        onehot = np.asarray(onehot)
        onehot = np.squeeze(onehot, axis=1)

        
        # 存放圖片的 ndarray list。
        images = []
        # 除掉 all same color image 用
        mask = np.ones(len(onehot), dtype=bool)

        for idx, fn in enumerate(csv.index):
            im_pth = os.path.join(os.getcwd(), fn)
            image = Image.open(im_pth)
            image = image.convert(mode='RGB')
            image = np.array(image) # image 轉成 ndarray.
            # chk if it all (0,0,0) pixel remove that!
            # Consider in initial period number of csv and image is the same.
            if self.chk_patch_is_dark(image):
                mask[idx] = False
                continue
            else:
                images.append(image)

        self.y = onehot[mask]
        self.X = np.array(images)

        if len(self.y) != len(self.X):
            # 會跳這個例外 代表 麻煩大了，篩選 pure color image 的取法有問題。
            raise Exception("len(self.y) = {}, len(self.X)={} are not the same!!\n出現此錯誤\'必須\'修改上方mask的算法!!"
                            .format(len(self.y),len(self.X)))

    def chk_patch_is_dark(self, image_patch):
        _1d = image_patch.flatten()
        tsize = _1d.__len__()
        if _1d[0]==int(np.sum(_1d) / tsize):
            return True
        else:
            return False

    def get_X_y(self):
        if self.X is None or self.y is None:
            raise Exception("X or y is None Object, load data failed !")
        return self.X, self.y