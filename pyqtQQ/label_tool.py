from PyQt5.QtWidgets import *
from mWindow import MyWidget
import os, glob


if __name__ == '__main__':
    """
    foldersPath_of_segmeted_image_sets 為切分qr code 的根目錄。
    """
    # 此路徑下 都是資料夾，每個資料夾內都是 已經切分過的 32*32 jpg, 且有csv在內。
    # 這要是 "根" 目錄(換句話說 這裡便全部都要是資料夾)
    foldersPath_of_segmeted_image_sets =\
        r'C:\Users\mslab\Desktop\segmented_qr_code'#r'D:\git-repo\fucker_papper\lab\data\segmented'# r'D:\Git\fucker_papper\lab\data\segmented'

    if os.path.isdir(foldersPath_of_segmeted_image_sets):
        # 判斷是否為根目錄
        if False in [os.path.isdir(_) for _ in glob.glob(foldersPath_of_segmeted_image_sets + '\\*')]:
            raise Exception("此目錄{}，並不是資料夾\'根\'目錄".format(foldersPath_of_segmeted_image_sets))
        print("根目錄存在。")
    else:
        raise Exception ("不存在此目錄:{}\n請再次檢查。".format(foldersPath_of_segmeted_image_sets))



    app = QApplication([])

    wdg = MyWidget(foldersPath_of_segmeted_image_sets)

    app.exec_()


