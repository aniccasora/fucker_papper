import sys, glob, math, csv, os
from PyQt5.QtWidgets import *
from PyQt5.QtGui import QIcon,QPixmap,QFont
from PyQt5.QtCore import Qt
from datetime import datetime

class MyWidget(QWidget):

    def __init__(self,root_folder):
        super().__init__()

        self.folder_list = glob.glob(root_folder + r'\*')  # 此 list 內的每個資料夾都裝著 qrcode patch.
        self.folder_ptr = -1  # 目前指到的 folder idx
        self.grid_btn_instance_list = [] # 存放著 grid layout  的 btn instance，因為沒有執行所以目前是空
        self.csv_pth = "" # qrcode patch 內的 csv path.
        self.__csv_labeled_number = -1 # 目前被標記的數量，根據 csv 做改動。
        if len(self.folder_list) > 0:
            self.folder_ptr = 0
        else:
            raise Exception("folder_ptr Error, not exists any folder in {}.".format(root_folder))

        # 沒寫在 self.initUI()，因為 msg_zone 要是成員變數的話要寫在 __init__(),個人推測...
        self.msg_zone = QLabel()

        self.initUI()

        # TODO: 要讓  folder_ptr 可以變動。

    def initUI(self):

        # layout 宣告
        self.total_layout = QVBoxLayout() # 總 layout 在裡面會垂直放置不同layout
        self.grid = QGridLayout()
        self.btn_layout = QHBoxLayout() # 裡面加上按鈕
        # 處理 grid layout
        # self.grid.maximumSize()

        # 處理 btn_layout 內的元件
        # 按鈕 1
        self.last_btn = QPushButton()
        self.last_btn.setIcon(QIcon(QPixmap(r".\icon\last_icon.jpg")))
        self.last_btn.clicked.connect(lambda state: self.last_folder())
        # 按鈕 2
        self.finish_btn = QPushButton()
        self.finish_btn.setIcon(QIcon(QPixmap(r".\icon\finish_icon.jpg")))
        self.load_image_to_grid() # 根據 成員變數 可得知目前要讀取的資料夾
        # self.finish_btn.clicked.connect(lambda state,
        #                                   x=self.grid_btn_instance_list,
        #                                   y=self.csv_pth: self.chkALL_allButton_instance_is_checked(x, y))
        self.finish_btn.clicked.connect(lambda state: self.chkALL_allButton_instance_is_checked())


        # 按鈕 3
        self.next_btn = QPushButton()
        self.next_btn.setIcon(QIcon(QPixmap(r".\icon\next_icon.jpg")))
        self.next_btn.clicked.connect(lambda state: self.next_folder())

        # 設定 btn_layout 的對其方式 並將 按鈕放入
        self.btn_layout.setAlignment(Qt.AlignBottom)
        self.btn_layout.addWidget(self.last_btn) # 塞按鈕
        self.btn_layout.addWidget(self.finish_btn) # 塞按鈕
        self.btn_layout.addWidget(self.next_btn)  # 塞按鈕

        # 疊加 layout，拼裝
        self.total_layout.addLayout(self.grid)
        self.total_layout.addLayout(self.btn_layout)


        # total layout 再加上一個 顯示訊息的區塊
        self.msg_zone.setFixedHeight(140)
        self.msg_zone.setFont(QFont("Microsoft JhengHei UI",14, QFont.Bold))
        self.msg_zone.setAlignment(Qt.AlignLeft|Qt.AlignTop) #
        self.msg_zone.setStyleSheet("border :2px solid black;")
        self.total_layout.addWidget(self.msg_zone)

        # 總 layout 給他加入到 QWidget 上.
        self.setLayout(self.total_layout)

        self.move(300,150)
        self.resize(500,500)
        #self.resize(self.sizeHint()) # can get a proper size automatically
        self.show()

    def load_image_to_grid(self):
        '''
        img_path_name : 請丟放著 patch qrcode 的 "資料夾" 路徑。
        '''
        self.clearLayout(self.grid)

        # 圖片資料夾
        path_name = self.folder_list[self.folder_ptr]
        # 更新窗口名稱
        title_n ="目前處理資料夾:"+path_name.split('\\')[-1]
        print(title_n)
        self.setWindowTitle(title_n+'\n')

        # 抓取ALL圖片的路徑
        images_pth = glob.glob(path_name + "\\*.jpg")
        # 抓取csv的路徑
        csv_pth = glob.glob(path_name + "\\*.csv")

        if len(csv_pth) != 1:
            print("發現{}筆 csv 檔案:\n".format(len(csv_pth))+str(csv_pth))
            raise Exception("Number of  csv file not 1.")
        # 確定 csv 只有 only one了
        self.csv_pth = csv_pth[0]

        # csv 的每一個 row 的 value，等同 csv 的 column = 2 .
        csv_datas = []
        # 開啟 CSV 檔案
        with open(self.csv_pth, newline='') as csvFile:

            # 自訂分隔符號：讀取 CSV 檔案內容
            rows = csv.reader(csvFile, delimiter=',')

            # 迴圈輸出 每一列
            for row in rows:
                csv_datas.append(row[1])

        # 取得已經標記的數量
        self.__csv_labeled_number = len([ e for e in csv_datas if int(e) == 1])
        csv_datas
        # 更新狀態列
        self.update_msg_zone("目前處理資料夾:" + self.get_current_folder_name() + '\n' +
                             "被標記數量:" + str(self.__csv_labeled_number))
        # 確認圖片"數量"可以組成一個正方形
        if not self.isqrt(len(images_pth)):
            raise Exception(str(len(images_pth)) + " is NOT sqrt!\nplease check folder image file number.")

        # 圖片按鈕的每個邊的數量
        side_number = math.floor(math.sqrt(len(images_pth)))


        self.grid_btn_instance_list = []

        buttons = {}

        for i in range(side_number):
            for j in range(side_number):
                # keep a reference to the buttons
                btn = QPushButton()  # 'row %d, col %d' % (i, j)

                # toggle 設定
                btn.setCheckable(True)

                if csv_datas[i * side_number + j] == '1':
                    btn.setChecked(True)

                # partial = std::bind 緊固定i與j， state仍然可被使用
                from functools import partial
                btn.clicked.connect(partial(self.clicked_event_partial, i, j))

                # -*-*-*-------------------------------
                img_seq = (i * side_number) + j
                # print(img_seq)
                img_pth = path_name + r"\qq_" + str(img_seq) + r".jpg"
                btn.setIcon(QIcon(QPixmap(img_pth)))
                buttons[(i, j)] = btn
                # add to the layout
                self.grid.addWidget(buttons[(i, j)], i, j)

                self.grid_btn_instance_list.append(btn)

                final_row = i

    def clearLayout(self, layout):
        while layout.count():
            child = layout.takeAt(0)
            if child.widget():
                child.widget().deleteLater()

    def addButtonFromFolder(self,folder_full_path):
        self.folder_name = folder_full_path.split('\\')[-1]

    # 用爆力法檢定 是否為平方數
    def isqrt(self,n):
        MAX_ = 1000**2
        if n >= MAX_:
            raise Exception("Too big n={}, but limit is MAX={}".format(n,MAX_))

        for i in range(math.ceil(math.sqrt(MAX_)+1)):
            if i**2 == n:
                return True
        return False

    def chkALL_allButton_instance_is_checked(self):
        '''

        '''
        folder_name = self.csv_pth.split('\\')[-2]

        fpth = '\\'.join(self.csv_pth.split('\\')[0:-1]) # full pth

        # 改檔名, uncheck-{檔案資料夾名}.csv 改成 {檔案資料夾名.cvs}
        after_change_name = folder_name

        # 要準備 create 的 csv
        new_create_csv = fpth + '\\' + after_change_name + '.csv'

        with open(new_create_csv, 'w', newline='') as file:
            writer = csv.writer(file)
            #print("labeled idx :")
            for idx,btn in enumerate(self.grid_btn_instance_list):
                if btn.isChecked(): # 給 1
                    #print('qq_'+str(idx)+".jpg,1")
                    writer.writerow(['qq_'+str(idx)+".jpg","1"])
                else: # 給 0
                    #print('qq_'+str(idx)+".jpg,0")
                    writer.writerow(['qq_' + str(idx) + ".jpg", "0"])
        # === csv processing over.


        L = folder_name+'.csv'
        R = self.csv_pth.split('\\')[-1] #  uncheck-{XX}.csv # old name
        if L != R:
            msg = "移除此檔案:"+ str(R)+'\n並新建:{}'.format(L)
            print(msg)
            if os.path.isfile(self.csv_pth):
                os.remove(self.csv_pth)
            else:
                新建: {}
                msg = "已經建立: {}".format(L)
        else:
            msg = "已存在"+folder_name+'.csv'+", 覆蓋此存檔。"
            print(msg)
        self.msg_zone.setText(msg+', '+str(datetime.now().strftime("%H:%M:%S")))
    def clicked_event_partial(self, i, j):
        # print(i,j)
        pass

    def update_msg_zone(self, msg):
        self.msg_zone.setText(msg)

    def next_folder(self):
        '''
        將 grid 的圖片替換至下一個 folder
        '''
        self.folder_ptr += 1
        self.folder_ptr %= len(self.folder_list)
        self.load_image_to_grid()


    def last_folder(self):
        self.folder_ptr -= 1
        self.folder_ptr %= len(self.folder_list)
        self.load_image_to_grid()

    def get_current_folder_name(self):
        return self.folder_list[self.folder_ptr].split('\\')[-1]